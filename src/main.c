// main.c file

/*
This file contains the UI code for the baker progam
*/

#include <stdio.h>
#include <string.h>
#include "baker.h"


	
int module_switcher( GtkWidget *widget, gpointer *stack)
{
	
	if(strcmp(gtk_widget_get_name(widget), "purchases_button") == 0 || 
	  strcmp(gtk_widget_get_name(widget), "ingredients_menu_item") == 0)
	{
		gtk_stack_set_visible_child( GTK_STACK(stack), gtk_stack_get_child_by_name(
		GTK_STACK(stack),"purchases_module"));
		return 0;
	}

	if(strcmp(gtk_widget_get_name(widget), "add_recipes") == 0 || 
	strcmp(gtk_widget_get_name(widget), "recipe_menu_item") == 0)
	{
		gtk_stack_set_visible_child( GTK_STACK(stack), gtk_stack_get_child_by_name(
		GTK_STACK(stack),"recipes_module"));
		return 0;
	}
	
	if(strcmp(gtk_widget_get_name(widget), "main_menu_item") == 0)
	{
		gtk_stack_set_visible_child( GTK_STACK(stack), gtk_stack_get_child_by_name(
		GTK_STACK(stack),"main_menu_module"));
		printf("main menu item has been clicked\n");
		return 0;
	}
	return 1;		
}

void fill_combo( GtkWidget* combo)
{

	//THESE NEED TO COME FROM DATABASE
		
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo),"1","lbs");
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo),"2","ounces")    ;
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo),"3","mls");
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo),"4","cups");
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo),"5","liters")    ;
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo),"6","quarts")    ;
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo),"7","pints");

		
}


int main (int argc, char **argv) { 


	gtk_init(&argc, &argv);


/********************Main Window*********************/ 
	GtkWidget* window;
	GtkWidget* main_box;
	GtkWidget* stack;
	GtkWidget* scrolled_window;

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL); 	
	gtk_window_set_default_size(GTK_WINDOW(window), 1300, 600);	

	scrolled_window = gtk_scrolled_window_new(NULL,NULL);

	main_box = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
	gtk_container_add(GTK_CONTAINER(scrolled_window),main_box);

	gtk_container_add(GTK_CONTAINER(window),scrolled_window);

	stack = gtk_stack_new(); //this will hold all the module screens
	
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);


/*********************Menu Bar**********************/
		
	GtkWidget* menu_bar;
	GtkWidget* file_menu_item;
	GtkWidget* modules_menu_item;
	GtkWidget* file_submenu;
	GtkWidget* modules_submenu;
	GtkWidget* exit_menu_item;
	GtkWidget* ingredients_menu_item;
	GtkWidget* recipe_menu_item;
	GtkWidget* customers_menu_item;
	GtkWidget* sales_menu_item;
	GtkWidget* reports_menu_item;
	GtkWidget* shopping_menu_item;
	GtkWidget* main_menu_item;

	// file menu 

	menu_bar = gtk_menu_bar_new();
	gtk_box_pack_start(GTK_BOX(main_box),menu_bar,FALSE,FALSE,0);

	file_menu_item = gtk_menu_item_new_with_label("File");
	gtk_container_add(GTK_CONTAINER(menu_bar),file_menu_item);

	file_submenu = gtk_menu_new();

	exit_menu_item = gtk_menu_item_new_with_label("Exit");	
	gtk_menu_attach(GTK_MENU(file_submenu),exit_menu_item,0,1,0,1);	

	gtk_menu_item_set_submenu(GTK_MENU_ITEM(file_menu_item),file_submenu);

	//connect calback functions
	g_signal_connect(exit_menu_item, "activate", G_CALLBACK(gtk_main_quit), NULL);


	//module menu 

	modules_menu_item = gtk_menu_item_new_with_label("Modules");
	gtk_container_add(GTK_CONTAINER(menu_bar),modules_menu_item);

	modules_submenu = gtk_menu_new();

	ingredients_menu_item = gtk_menu_item_new_with_label("Ingredients");
	gtk_widget_set_name(ingredients_menu_item, "ingredients_menu_item");	

	recipe_menu_item = gtk_menu_item_new_with_label("Recipes");
	gtk_widget_set_name(recipe_menu_item, "recipe_menu_item");	

	customers_menu_item = gtk_menu_item_new_with_label("Customers");
	sales_menu_item = gtk_menu_item_new_with_label("Sales");
	reports_menu_item = gtk_menu_item_new_with_label("Reports");
	shopping_menu_item = gtk_menu_item_new_with_label("Shopping");

	main_menu_item = gtk_menu_item_new_with_label("Main Menu");
	gtk_widget_set_name(main_menu_item, "main_menu_item");	
	

	gtk_menu_item_set_submenu(GTK_MENU_ITEM(modules_menu_item),modules_submenu);

	gtk_menu_attach(GTK_MENU(modules_submenu),ingredients_menu_item,0,1,0,1);	
	gtk_menu_attach(GTK_MENU(modules_submenu),recipe_menu_item,0,1,1,2);	
	gtk_menu_attach(GTK_MENU(modules_submenu),customers_menu_item,0,1,2,3);	
	gtk_menu_attach(GTK_MENU(modules_submenu),sales_menu_item,0,1,3,4);	
	gtk_menu_attach(GTK_MENU(modules_submenu),reports_menu_item,0,1,4,5);	
	gtk_menu_attach(GTK_MENU(modules_submenu),shopping_menu_item,0,1,5,6);	
	gtk_menu_attach(GTK_MENU(modules_submenu),main_menu_item,0,1,6,7);	


	//connect callback functions 
	g_signal_connect(ingredients_menu_item, "activate", G_CALLBACK(module_switcher), stack);
	g_signal_connect(main_menu_item, "activate", G_CALLBACK(module_switcher), stack);
	g_signal_connect(recipe_menu_item, "activate", G_CALLBACK(module_switcher), stack);

/************************Stack**********************************/


	gtk_box_pack_start(GTK_BOX(main_box),stack,FALSE,FALSE,0);


/*************************Main Menu Module***************************/

	GtkWidget* main_menu;
	GtkWidget* left_box;
	GtkWidget* right_box;
	GtkWidget* purchases_button;
	GtkWidget* add_recipes;
	GtkWidget* customers_button;
	GtkWidget* sales_button;
	GtkWidget* reports_button;
	//GtkWidget* shopping_button;
	

	main_menu = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
	gtk_stack_add_named(GTK_STACK(stack), main_menu, "main_menu_module");

	left_box = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
	right_box = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);

	
	gtk_box_pack_start(GTK_BOX(main_menu),left_box,FALSE,FALSE,0);
	gtk_box_pack_start(GTK_BOX(main_menu),right_box,FALSE,FALSE,0);


	purchases_button = gtk_button_new_with_label("Purchases");
	gtk_widget_set_name(purchases_button, "purchases_button");	
	gtk_box_pack_start(GTK_BOX(left_box),purchases_button,FALSE,FALSE,0);

	add_recipes = gtk_button_new_with_label("Add Recipes");	
	gtk_widget_set_name(add_recipes, "add_recipes");	
	gtk_box_pack_start(GTK_BOX(left_box),add_recipes,FALSE,FALSE,0);

	customers_button = gtk_button_new_with_label("Customers");	
	gtk_box_pack_start(GTK_BOX(left_box),customers_button,FALSE,FALSE,0);

	sales_button = gtk_button_new_with_label("Sales");	
	gtk_box_pack_start(GTK_BOX(right_box),sales_button,FALSE,FALSE,0);

	reports_button = gtk_button_new_with_label("Reports");	
	gtk_box_pack_start(GTK_BOX(right_box),reports_button,FALSE,FALSE,0);

	//connect to callback functions 
	g_signal_connect(purchases_button, "clicked",G_CALLBACK(module_switcher),stack);
	g_signal_connect(add_recipes, "clicked",G_CALLBACK(module_switcher),stack);


/***********************Purchases Module***************************/

	GtkWidget* purchases_module;
	GtkWidget* purchases_input_grid;
	GtkWidget* ingredient_input_entry_label;
	GtkWidget* ingredient_input_entry;
	GtkWidget* quantity_input_entry_label;
	GtkWidget* quantity_input_entry;
	GtkWidget* uom_input_entry_label;
	GtkWidget* uom_input_entry;
	GtkWidget* price_input_entry_label;
	GtkWidget* price_input_entry;
	GtkWidget* purchase_save_button;
	GtkWidget* purchases_view;


	purchases_module = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
	gtk_widget_set_name(purchases_module, "purchases_module");	
	gtk_stack_add_named(GTK_STACK(stack), purchases_module, "purchases_module");
	

	//Purchases Input Grid

	purchases_input_grid = gtk_grid_new();
	gtk_box_pack_start(GTK_BOX(purchases_module),purchases_input_grid,FALSE,FALSE,0);

	ingredient_input_entry_label = gtk_label_new("Ingredient");	
	gtk_grid_attach(GTK_GRID(purchases_input_grid),ingredient_input_entry_label,0,0,1,1);	
	
	ingredient_input_entry = gtk_entry_new();
	gtk_grid_attach(GTK_GRID(purchases_input_grid),ingredient_input_entry,0,1,1,1);	

	quantity_input_entry_label = gtk_label_new("Quantity");	
	gtk_grid_attach(GTK_GRID(purchases_input_grid),quantity_input_entry_label,1,0,1,1);	
	
	quantity_input_entry = gtk_entry_new();
	gtk_grid_attach(GTK_GRID(purchases_input_grid),quantity_input_entry,1,1,1,1);	

	uom_input_entry_label = gtk_label_new("UOM");	
	gtk_grid_attach(GTK_GRID(purchases_input_grid),uom_input_entry_label,2,0,1,1);	
	
	uom_input_entry = gtk_combo_box_text_new_with_entry();
	fill_combo(uom_input_entry);	
	gtk_grid_attach(GTK_GRID(purchases_input_grid),uom_input_entry,2,1,1,1);	

	price_input_entry_label = gtk_label_new("Price");	
	gtk_grid_attach(GTK_GRID(purchases_input_grid),price_input_entry_label,3,0,1,1);	
	
	price_input_entry = gtk_entry_new();
	gtk_grid_attach(GTK_GRID(purchases_input_grid),price_input_entry,3,1,1,1);	

	purchase_save_button = gtk_button_new_with_label("Save Ingredient");
	gtk_grid_attach(GTK_GRID(purchases_input_grid),purchase_save_button,4,1,1,1);

	//Purchases view.
	purchases_view = ingredients_display();
	gtk_box_pack_start(GTK_BOX(purchases_module),purchases_view,FALSE,FALSE,0);

	//connect to callback functions
	g_signal_connect(purchase_save_button, "clicked", G_CALLBACK(save_purchase), purchases_input_grid);
	g_signal_connect(purchase_save_button, "clicked", G_CALLBACK(update_purchases_view), purchases_view);

/******************Recipes Module*************************************/
	
	GtkWidget* recipes_module;
	GtkWidget* recipes_grid;
	GtkWidget* recipes_name_entry_label;
	GtkWidget* recipes_name_entry;
	GtkWidget* recipes_author_entry_label;
	GtkWidget* recipes_author_entry;
	GtkWidget* recipes_save_button;
	GtkWidget* recipes_instructions_scrolled_window;
	GtkWidget* recipes_instructions_label;
	GtkWidget* recipes_instructions;
	GtkTextBuffer* recipes_instructions_buffer;

	recipes_module = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
	gtk_widget_set_name(recipes_module, "recipes_module");	
	gtk_stack_add_named(GTK_STACK(stack), recipes_module, "recipes_module");

	recipes_grid = gtk_grid_new();
	gtk_box_pack_start(GTK_BOX(recipes_module),recipes_grid,FALSE,FALSE,0);

	char ing_label[14] = "Ingredient ";
	char line_num[3] = "1";
	int tens;

	recipes_name_entry_label = gtk_label_new("Recipe Name");
	gtk_grid_attach(GTK_GRID(recipes_grid),recipes_name_entry_label,0,0,2,1);	
	recipes_name_entry = gtk_entry_new();
	gtk_grid_attach(GTK_GRID(recipes_grid),recipes_name_entry,0,1,2,1);
	gtk_entry_set_width_chars(GTK_ENTRY(recipes_name_entry),50);
	
	recipes_author_entry_label = gtk_label_new("Recipe Author");
	gtk_grid_attach(GTK_GRID(recipes_grid),recipes_author_entry_label,2,0,1,1);	
	recipes_author_entry = gtk_entry_new();
	gtk_grid_attach(GTK_GRID(recipes_grid),recipes_author_entry,2,1,1,1);


	recipes_save_button = gtk_button_new_with_label("Save Recipe ");
	gtk_grid_attach(GTK_GRID(recipes_grid),recipes_save_button,3,1,1,1);

	recipes_instructions_label = gtk_label_new("Instructions");
	gtk_grid_attach(GTK_GRID(recipes_grid),recipes_instructions_label,3,2,1,1);	

	recipes_instructions_scrolled_window = gtk_scrolled_window_new(NULL,NULL);
	gtk_widget_set_name(recipes_instructions_scrolled_window,"recipes_instructions_scrolled_window");

	recipes_instructions = gtk_text_view_new();
	gtk_widget_set_name(recipes_instructions,"recipes_instructions1");

	recipes_instructions_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (recipes_instructions));
	gtk_container_add(GTK_CONTAINER (recipes_instructions_scrolled_window),recipes_instructions);
	gtk_grid_attach(GTK_GRID(recipes_grid),recipes_instructions_scrolled_window,3,3,2,17);
	gtk_scrolled_window_set_min_content_width (GTK_SCROLLED_WINDOW(recipes_instructions_scrolled_window),600);

	//add individual ingredient fields to recipes grid

	for (int i = 0; i < NUM_INGREDIENTS; i ++)
	{
		//convert i to a string 
		tens = (i + 1) / 10;
		if(tens > 0)
		{
			line_num[0] = tens + 48;  
			line_num[1] = i + 49 - tens * 10; 

		}else {
			
			line_num[0] = i + 49; 
		}
		
		strcat(ing_label,line_num);		

		ingredient_lines[i].label = gtk_label_new(ing_label); 
		gtk_grid_attach(GTK_GRID(recipes_grid),ingredient_lines[i].label,0,2+2*i,1,1);	
		ingredient_lines[i].ingredient = gtk_entry_new();
		gtk_grid_attach(GTK_GRID(recipes_grid),ingredient_lines[i].ingredient,0,3+2*i,1,1);	

		ingredient_lines[i].quantity_label = gtk_label_new("Quantity");
		gtk_grid_attach(GTK_GRID(recipes_grid),ingredient_lines[i].quantity_label,1,2+2*i,1,1);	
		ingredient_lines[i].quantity = gtk_entry_new();
		gtk_grid_attach(GTK_GRID(recipes_grid),ingredient_lines[i].quantity,1,3+2*i,1,1);	

		ingredient_lines[i].uom_label = gtk_label_new("Unit of Measure");
		gtk_grid_attach(GTK_GRID(recipes_grid),ingredient_lines[i].uom_label,2,2+2*i,1,1);	
		ingredient_lines[i].uom = gtk_entry_new();
		gtk_grid_attach(GTK_GRID(recipes_grid),ingredient_lines[i].uom,2,3+2*i,1,1);	

		// reset ingredient label
		ing_label[11] = '\0';


	}//end of for loop

	
	g_signal_connect(recipes_save_button, "clicked", G_CALLBACK(save_recipe), recipes_grid);

/***********************display to screen***************************/

	gtk_widget_show_all(window);

	gtk_main();

	return 0;
}
