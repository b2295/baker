#pragma once
#include <mysql.h>
#include <gtk/gtk.h>
#define	NUM_INGREDIENTS 10 

int dbConnect();

int dbIn( char *statement);
MYSQL_RES *dbOut( char *statement);

void dbClose();

void save_purchase(GtkWidget *button1, gpointer user_data);

GtkWidget* ingredients_display(void);

void update_purchases_view(GtkWidget* button, GtkWidget* ingredient_view);

void save_recipe(GtkWidget *save_button, gpointer *recipes_grid);


struct ingredient_line {

                GtkWidget* label;
                GtkWidget* ingredient;
                GtkWidget* quantity_label;
                GtkWidget* quantity;
                GtkWidget* uom_label;
                GtkWidget* uom;
		GtkWidget* note_label;
		GtkWidget* note;

        };  

struct ingredient_line ingredient_lines[NUM_INGREDIENTS];

