/*filename: baker_includes.c*/

#include <stdio.h>
#include <time.h>
#include "baker.h"


static void print_error (MYSQL *conn, char *message);
static MYSQL *conn;

char* get_sql_date()
{

	char* date = malloc(11);  
	struct tm *ltm;

	time_t now = time(NULL);
	ltm = localtime(&now);
	strftime(date,11,"%Y-%m-%d" ,ltm);	
	printf("strftime created: %s\n",date);
	return date;
}

int dbConnect(){

	static char *opt_host_name = NULL;
	static char opt_user_name[] = "baker";
	static char opt_password[] = "baker";
	static unsigned int opt_port_num = 0;
	static char *opt_socket_name = NULL;
	static char opt_db_name[] = "baker";
	static unsigned int opt_flags = 0;


	//initialize connection handler

        conn = mysql_init(NULL);

        if (conn == NULL)
	{
		print_error (NULL, "mysql_init() failed (probably out of memory)");
                exit(1);

        }

	//connect to server

	if (mysql_real_connect (conn, opt_host_name, opt_user_name, opt_password, opt_db_name, opt_port_num, opt_socket_name, opt_flags) == NULL)
	{
		print_error (conn, "mysql_real_connect() failed");
		mysql_close (conn);
		exit (1);		
	}

	return 0;


}//end of dbConnect
 

int dbIn( char *statement)
{
	dbConnect();
	if (mysql_query(conn, statement))
	{
		print_error (conn, "mysql_query() failed");
		mysql_close (conn);
		exit (1);
	}
	return 0;
}

MYSQL_RES *dbOut( char *statement)
{	
	dbConnect();
	if (mysql_query(conn, statement))
	{
		print_error (conn, "mysql_query() failed");
		mysql_close (conn);
		MYSQL_RES *result = NULL;
		return result;
		
	}

	MYSQL_RES *result = mysql_store_result(conn);

	if (result == NULL)
	{
		print_error(conn, "mysql_store_result() failed");
		mysql_close (conn);
	}
	return result;

}//end of dbOut


	
void dbClose()
{
	//dsconnect from server
	mysql_close (conn);
	exit(0);
}

static void print_error (MYSQL *conn, char *message)
{
	fprintf (stderr, "%s\n", message);
	if (conn != NULL)
	{
#if MYSQL_VERSION_ID >= 40101
		fprintf (stderr, "Error %u (%s): %s\n", mysql_errno(conn), mysql_sqlstate(conn), mysql_error (conn));		
#else
		fprintf (stderr, "Error %u: %s\n",mysql_errno(conn), mysql_error (conn));		
#endif
	}

}
	

void save_purchase(GtkWidget *button1, gpointer user_data)
{
	//store ingredient data from user
	GtkWidget *ingredientEntry;
	GtkWidget *ingredientQuantityEntry;
	GtkWidget *uomCombo;
	GtkWidget *ingredientPrice;
	GtkWidget *uomComboEntry;
	
	const char *ingredient;
	const char *quantity;
	const char *price;
	const char *uom;

	//get date
	char *date = get_sql_date();
	
	// get input fields text	
	ingredientEntry = gtk_grid_get_child_at( user_data,0,1); 	
	ingredient = gtk_entry_get_text(GTK_ENTRY(ingredientEntry));

	ingredientQuantityEntry = gtk_grid_get_child_at( user_data,1,1); 	
	quantity = gtk_entry_get_text(GTK_ENTRY(ingredientQuantityEntry));

	uomCombo = gtk_grid_get_child_at( user_data,2,1);
	uomComboEntry = gtk_bin_get_child(GTK_BIN(uomCombo));
	uom = gtk_entry_get_text(GTK_ENTRY(uomComboEntry));	

	ingredientPrice = gtk_grid_get_child_at( user_data,3,1); 	
	price = gtk_entry_get_text(GTK_ENTRY(ingredientPrice));
	

	//build SQL INSERT statement
	/****NEED TO PREVENT SQL INJECTION ATTACK HERE SOMEHOW******/

	char *singleQuote = "'";	
	char *comma = ",";
	char *close = ");";
	char statement[200] = "INSERT INTO itemtrans (name, quantity, uom, price, date) VALUES ('";

	strcat(statement, ingredient);
	strcat(statement, singleQuote);
	strcat(statement, comma);
	strcat(statement, quantity);
	strcat(statement, comma);
	strcat(statement, singleQuote);
	strcat(statement, uom);
	strcat(statement, singleQuote);
	strcat(statement, comma);
	strcat(statement, price);
	strcat(statement, comma);
	strcat(statement, singleQuote);
	strcat(statement, date);
	strcat(statement, singleQuote);
	strcat(statement, close);
	
	//enter into database
	printf("enterting statement into database \n");
	printf("statement is: %s\n", statement);
	dbIn(statement); 
	
	// clear inputs
	
	gtk_entry_set_text(GTK_ENTRY(ingredientEntry), "");
	gtk_entry_set_text(GTK_ENTRY(ingredientQuantityEntry), "");
	gtk_entry_set_text(GTK_ENTRY(uomComboEntry), "");
	gtk_entry_set_text(GTK_ENTRY(ingredientPrice), "");
}

enum
	{
		ID_COLUMN = 0,
		NAME_COLUMN,
		QUANTITY_COLUMN,
		UOM_COLUMN,
		PRICE_COLUMN,
		DATE_COLUMN,
		N_COLUMNS
	};

GtkWidget *ingredients_display (void)
{
	GtkListStore *store = gtk_list_store_new (	N_COLUMNS, 
							G_TYPE_STRING, // ID
							G_TYPE_STRING, // Name
							G_TYPE_STRING, // Quantity
							G_TYPE_STRING, // UOM
							G_TYPE_STRING, // Price 	
							G_TYPE_STRING); // Date

	// create SQL statement to get ingredient data
	char get_ingredients_statement[200] = "SELECT * FROM itemtrans;";	

	// insert records into GtkTreeStore

		
	MYSQL_RES *result;
	result = dbOut(get_ingredients_statement);

	
	if (result == NULL)
	{	
		printf("dbOut() failed\n");
		return NULL;
	}
	
	
	int num_fields = mysql_num_fields(result);
        MYSQL_ROW row;
	GtkTreeIter iter;

	int i = 0;

        while((row = mysql_fetch_row(result)))
        {
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,
					ID_COLUMN, row[ID_COLUMN],
					NAME_COLUMN, row[NAME_COLUMN],
					QUANTITY_COLUMN, row[QUANTITY_COLUMN],
					UOM_COLUMN, row[UOM_COLUMN],
					PRICE_COLUMN, row[PRICE_COLUMN],
					DATE_COLUMN, row[DATE_COLUMN],
					-1); 
	
        }

	//Create view and add columns to view

	GtkWidget *view = gtk_tree_view_new();
	GtkWidget *scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	GtkCellRenderer *renderer; 

	//ID column

	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
							-1,
							"ID",
							renderer,
							"text", ID_COLUMN,
							NULL);
	// Name column
	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
							-1,
							"Name",
							renderer,
							"text", NAME_COLUMN,
							NULL);

	// Quantity column
	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
							-1,
							"Quntity",
							renderer,
							"text", QUANTITY_COLUMN,
							NULL);
	//UOM column
	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
							-1,
							"UOM",
							renderer,
							"text", UOM_COLUMN,
							NULL);
	//Price column
	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
							-1,
							"Price",
							renderer,
							"text", PRICE_COLUMN,
							NULL);
	//Date column
	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
							-1,
							"Date",
							renderer,
							"text", DATE_COLUMN,
							NULL);


	//Add store to view

	gtk_tree_view_set_model (GTK_TREE_VIEW(view), GTK_TREE_MODEL(store));	
	g_object_unref (store);


	//Add view to scrolled window so it will have a scrollbar
	gtk_container_add(GTK_CONTAINER(scrolled_window),view);
	gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(scrolled_window),600);

	return scrolled_window;
}
	
void update_purchases_view(GtkWidget* button, GtkWidget* purchases_view)
{
	purchase_view = ingredients_display();	
}

void save_recipe(GtkWidget *save_button, gpointer *recipes_grid)
{

	//get recipe name
	GtkWidget* recipes_name_entry;	
	const gchar* recipes_name;
	
	recipes_name_entry = gtk_grid_get_child_at(GTK_GRID(recipes_grid),0,1); 
	recipes_name =  gtk_entry_get_text(GTK_ENTRY(recipes_name_entry));

	//get recipe author
	GtkWidget* recipes_author_entry;	
	const gchar* recipes_author;
	
	recipes_author_entry = gtk_grid_get_child_at(GTK_GRID(recipes_grid),2,1); 
	recipes_author =  gtk_entry_get_text(GTK_ENTRY(recipes_author_entry));

	//get recipe instructions
	GtkWidget* recipes_instructions_scrolled_window;
	GtkWidget* recipes_instructions_viewport;	
	GtkTextBuffer* recipes_instructions_buffer;
	gchar* recipes_instructions;
	GtkTextIter start, end;

	
	recipes_instructions_scrolled_window = gtk_grid_get_child_at(GTK_GRID(recipes_grid),3,3);
	recipes_instructions_viewport = gtk_bin_get_child(GTK_BIN(recipes_instructions_scrolled_window));
	recipes_instructions_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(recipes_instructions_viewport));
	gtk_text_buffer_get_bounds(recipes_instructions_buffer,&start,&end);
	recipes_instructions = gtk_text_buffer_get_text(recipes_instructions_buffer,&start,&end,0);

	//get date
	const gchar *date = get_sql_date();

	//build SQL INSERT statement
	char *singleQuote = "'";	
	char *comma = ",";
	char *close = ");";
	char *semicolon = ";";
	char statement[200] = "INSERT INTO recipes (name, author,description, date) VALUES ('";

	strcat(statement, recipes_name);
	strcat(statement, singleQuote);
	strcat(statement, comma);
	strcat(statement, singleQuote);
	strcat(statement, recipes_author);
	strcat(statement, singleQuote);
	strcat(statement, comma);
	strcat(statement, singleQuote);
	strcat(statement, recipes_instructions);
	strcat(statement, singleQuote);
	strcat(statement, comma);
	strcat(statement, singleQuote);
	strcat(statement, date);
	strcat(statement, singleQuote);
	strcat(statement, close);
	
	//enter into database
	dbIn(statement); 

	//get ingredient lines	

	const gchar* ingredient;
	const gchar* quantity;
	const gchar* uom;

	for(int i = 0; i < NUM_INGREDIENTS; i++)
	{
		if (*(ingredient = gtk_entry_get_text(GTK_ENTRY(ingredient_lines[i].ingredient))) == 0)
		{
			continue; // no ingredient was entered in field. Skip to the next

		}
		char ingredient_statement[100] = "SELECT ID FROM ingredients WHERE name ='";
		quantity = gtk_entry_get_text(GTK_ENTRY(ingredient_lines[i].quantity));
		uom = gtk_entry_get_text(GTK_ENTRY(ingredient_lines[i].uom));

		//build select statement
		strcat(ingredient_statement,ingredient);
		strcat(ingredient_statement,singleQuote);
		strcat(ingredient_statement,semicolon);
 
		printf("The select statement is: %s\n",ingredient_statement);	

		

		//find ingredient from ingredients table
		MYSQL_RES *result = dbOut(ingredient_statement);

		//if no ingredient exist add to table 

		int num_rows = mysql_num_rows(result);
		
	
		if ( num_rows == 0)
		{
	
			printf("%s not found in ingredient table. Adding it now\n", ingredient);
			// create insert statement
			char insert_statement[100] = "INSERT INTO ingredients (name,date) VALUES ('";
			strcat(insert_statement,ingredient);
			strcat(insert_statement,singleQuote);
			strcat(insert_statement,comma);
			strcat(insert_statement,singleQuote);
			strcat(insert_statement,date);
			strcat(insert_statement,singleQuote);
			strcat(insert_statement,close);
	
			printf("The insert statement is: %s\n",insert_statement);	

			dbIn(insert_statement);
			
				
		}else{

			//get ingredient ID
			MYSQL_ROW row;
			


			row = mysql_fetch_row(result);

			printf("row[0] is %s\n",row[0]);

		}

	}//end of for loop 

}
